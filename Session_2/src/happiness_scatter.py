
import matplotlib as mp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv, sys

def get_data(filename):
    """
    This function extracts data from a csv file.
    Args:
        filename (string): Name of the csv data file to be read from.

    Returns:
        Pandas DataFrame: storing the contents of the csv file
    """
    return pd.read_csv(filename)

def data_plot(data_points):
    data_points = data_points[:40]
    plt.grid(True)
    plt.bar(data_points['Country'], data_points['Economy (GDP per Capita)'], color = "red")
    plt.scatter([country[:3] for country in data_points['Country']], data_points['Trust (Government Corruption)'])
    plt.show()
    
data_plot(get_data(sys.argv[1]))